package model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by EncureIT on 27-12-2017.
 */

public class Price {

    public Price(String BTC, String ETH, String BCH, String XRP, String LTC, double MIOTA, double OMG, double GNT) {
        this.BTC = BTC;
        this.ETH = ETH;
        this.BCH = BCH;
        this.XRP = XRP;
        this.LTC = LTC;
        this.MIOTA = MIOTA;
        this.OMG = OMG;
        this.GNT = GNT;
    }

    @SerializedName("BTC")
    public String BTC;
    @SerializedName("ETH")
    public String ETH;
    @SerializedName("BCH")
    public String BCH;
    @SerializedName("XRP")
    public String XRP;
    @SerializedName("LTC")
    public String LTC;
    @SerializedName("MIOTA")
    public double MIOTA;
    @SerializedName("OMG")
    public double OMG;
    @SerializedName("GNT")
    public double GNT;


    public String getBTC() {
        return BTC;
    }

    public void setBTC(String BTC) {
        this.BTC = BTC;
    }

    public String getETH() {
        return ETH;
    }

    public void setETH(String ETH) {
        this.ETH = ETH;
    }

    public String getBCH() {
        return BCH;
    }

    public void setBCH(String BCH) {
        this.BCH = BCH;
    }

    public String getXRP() {
        return XRP;
    }

    public void setXRP(String XRP) {
        this.XRP = XRP;
    }

    public String getLTC() {
        return LTC;
    }

    public void setLTC(String LTC) {
        this.LTC = LTC;
    }

    public double getMIOTA() {
        return MIOTA;
    }

    public void setMIOTA(double MIOTA) {
        this.MIOTA = MIOTA;
    }

    public double getOMG() {
        return OMG;
    }

    public void setOMG(double OMG) {
        this.OMG = OMG;
    }

    public double getGNT() {
        return GNT;
    }

    public void setGNT(double GNT) {
        this.GNT = GNT;
    }



}
