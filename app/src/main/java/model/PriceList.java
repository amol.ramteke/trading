package model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by EncureIT on 27-12-2017.
 */

public class PriceList {

    @SerializedName("prices")
    private Price prices;

    public Price getPrices() {
        return prices;
    }

    public void setPrices(Price prices) {
        this.prices = prices;
    }
}
