package custom_font;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import utility.Constants;


/**
 * Created by amol on 27-12-17.
 */
@SuppressWarnings("ALL")
public class ButtonBold extends android.support.v7.widget.AppCompatButton {

    private Context mContext;

    public ButtonBold(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public ButtonBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public ButtonBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }


    public void init(){
        Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), Constants.FONT_DIR+ Constants.FONT_BOLD);
        setTypeface(typeface);
    }
}
