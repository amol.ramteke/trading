package nav_drawer;

import android.annotation.SuppressLint;
import android.content.Context;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.example.encureit.trading.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import custom_font.TextViewRegular;


/**
 * Created by amol on 27-12-17.
 */

public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.ViewHolder> {

    private String mNavTitles[];
    private int mIcons[];
    private NavItemClicked navItemClicked;

    public NavigationDrawerAdapter(Context mContext, NavItemClicked navItemClicked, String Titles[], int Icons[]) {
       // Context mContext1 = mContext;
        this.navItemClicked = navItemClicked;
        mNavTitles = Titles;
        mIcons = Icons;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvForMenuItem)
        TextViewRegular tvForMenuItem;
        @BindView(R.id.ivForMenuItem)
        ImageView ivForMenuItem;
        @BindView(R.id.llForNavItem)
        LinearLayout llForNavItem;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.nav_drawer_row, parent, false);

        return new ViewHolder(v);

    }

    @SuppressLint("InflateParams")
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.tvForMenuItem.setText(mNavTitles[holder.getAdapterPosition()]);
        holder.ivForMenuItem.setImageResource(mIcons[position]);

        holder.llForNavItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navItemClicked.navItemClicked(mNavTitles[holder.getAdapterPosition()], holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mNavTitles.length;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
