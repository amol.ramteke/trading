package nav_drawer;

/**
 * Created by Mankesh17 on 04-Aug-17.
 */

public interface NavItemClicked {

    void navItemClicked(String name, int position);

}
