package webUtil;

import android.content.Context;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import utility.Constants;
import utility.SharePreferenceSingleton;

/**
 * Created by EncureIT on 27-12-2017.
 */

public class APIServiceFactory {

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static SharePreferenceSingleton sharedpreferences;


    public static <S> S createService(Class<S> serviceClass, final Context context){
        sharedpreferences = SharePreferenceSingleton.getSingletonInstance();
        sharedpreferences.setPref(context);
        sharedpreferences.setEditor1();

        Retrofit.Builder builder=new Retrofit.Builder().baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());
        OkHttpClient client=httpClient.build();
        Retrofit retrofit=builder.client(client).build();
        return retrofit.create(serviceClass);

    }


}
