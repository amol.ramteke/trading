package webUtil;

import model.Price;
import model.PriceList;
import retrofit2.Call;
import retrofit2.http.GET;
import utility.Constants;

/**
 * Created by EncureIT on 27-12-2017.
 */

public interface AccountService {

    @GET(Constants.GET_PRICE_LIST_KOINEX)
    Call<PriceList> getKoinexPrice();

}
