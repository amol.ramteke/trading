package webUtil;

/**
 * Created by EncureIT on 27-12-2017.
 */

public interface APICallback {
    <T> void onSuccess(T serviceResponse);
    <T> void onFailure(T apiErrorModel, T extras);

}
