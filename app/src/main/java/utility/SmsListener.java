package utility;

/**
 * Created by EncureIT on 24-01-2018.
 */

public interface SmsListener {
    public void messageReceived(String textMsg);
}
