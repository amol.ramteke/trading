package utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.Map;

/**
 * Created by amol on 27/12/17.
 */
@SuppressWarnings("ALL")
public class SharePreferenceSingleton {
    private static SharedPreferences pref;
    private static SharePreferenceSingleton singleton;
    private Editor editor;

    private SharePreferenceSingleton() {
    }

    /* Static 'instance' method */
    public static SharePreferenceSingleton getSingletonInstance() {
        if (null == singleton) {
            singleton = new SharePreferenceSingleton();
        }
        return singleton;
    }

    public int getValueFromPrefInt(String key) {
        return pref.getInt(key, 0);
    }

    public void setValueToPrefInt(String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }

    public String getValueFromPrefString(String key) {
        return pref.getString(key, "");
    }

    public void setValueToPrefString(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public void setValueToPrefBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public boolean getValueFromPrefBoolean(String key) {
        return pref.getBoolean(key, false);
    }

    @SuppressWarnings("static-access")
    public void setPref(Context context) {
        this.pref = context.getSharedPreferences("SecurityAppPref",
                context.MODE_PRIVATE);
    }

    public void setEditor1() {
        this.editor = pref.edit();
    }


    public Map<String, ?> getAll()
    {
        return pref.getAll();
    }



}
