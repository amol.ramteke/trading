package utility;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;


import com.example.encureit.trading.R;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Mankesh71 on 6/5/2017.
 */
@SuppressWarnings("ALL")
public class CustomProgressDialog {
    private SweetAlertDialog mDialog;
    private static CustomProgressDialog pDialog;
    private CustomProgressDialog() {
    }

    public static synchronized CustomProgressDialog getInstance() {
        if (pDialog == null) {
            pDialog = new CustomProgressDialog();
        }
        return pDialog;
    }

    public  void showDialog(Context context, String msg, int processType) {

        String msgString ;
        if(((AppCompatActivity) context).isFinishing())
        {
            return;
        }

        switch (processType) {
            case Constants.PROGRESS_TYPE:
                if (mDialog==null) {
                    mDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
                    mDialog.getProgressHelper().setBarColor(Color.parseColor("#0071AA"));
                    if (msg != null && !"".equals(msg)) {
                        mDialog.setTitleText(msg);
                    } else {
                        mDialog.setTitleText(context.getResources().getString(R.string.loading));
                    }
                    mDialog.setCancelable(false);
                    if (!mDialog.isShowing()) {
                        mDialog.show();
                    }
                }
                break;
            /*case Constants.ERROR_TYPE:
                if (msg != null && !"".equals(msg)) {
                    msgString=msg;
                } else {
                    msgString= context.getResources().getString(R.string.some_thing_went_wrong);
                }
                new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(msgString)
                        .show();
                break;*/

            case Constants.SUCCESS_TYPE:
                if (msg != null && !"".equals(msg)) {
                    msgString=msg;
                } else {
                    msgString="Done";
                }
                new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText("Success!")
                        .setContentText(msgString)
                        .show();
                break;

            case Constants.WARNING_TYPE:
                if (msg != null && !"".equals(msg)) {
                    msgString=msg;
                } else {
                    msgString= context.getResources().getString(R.string.some_thing_went_wrong);
                }
                new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Oops!")
                        .setContentText(msgString)
                        .show();
                break;
        }
    }

    public void dismissDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
            mDialog = null;
        }
    }

}
