package utility;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Base64;

import java.io.ByteArrayOutputStream;

/**
 * Created by amol on 31/7/17.
 */

public class Constants {

    public static final String NETWORK_ERROR = "Network is not available. Establish network connection.";
    public static final String CONNECTION_TIMEOUT = "Connection Timeout!";
    public static final String TRY_AGAIN_MESSAGE = "Try Again!";
    public static final String STATUS_MESSAGE = "message";
    public static final String TAG_DATA = "data";
    public static final String TAG_FROM_GALLERY = "fromGallery";
    public static final String TAG_FROM_CAMERA = "fromCamera";

    public final static String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    public final static String mobilePattern = "[0-9]{9}";

    /*Sweet alert*/
    public static final int ERROR_TYPE = 1;
    public static final int SUCCESS_TYPE = 2;
    public static final int WARNING_TYPE = 3;
    public static final int PROGRESS_TYPE = 5;



    /***
     * Font constant
     */
    public static final String FONT_DIR = "font/";
    public static final String FONT_BOLD = "Raleway-Bold.ttf";
    public static final String FONT_REGULAR = "Raleway-Regular.ttf";

    /****
    * Base and Other URL need in app
    * */
    public final static String BASE_URL = "https://koinex.in/api/";
    public final static String GET_PRICE_LIST_KOINEX = "ticker";
    public final static String REGISTER_URL = "http://";
    public final static String LOGIN_URL = "http://";


    /***
     * Login
     * */
    public static final String LOGIN_SUCCESS = "LoginSuccess";
    public static final String TAG_TERMS_CONDITION_PATH = "http://www.google.com";


    /***
     * For upload image to server using Base64Encoding
     * */
    public static String encodedImage(Bitmap photo) {
        photo = Bitmap.createScaledBitmap(photo, 800, 800, true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] fileByte = stream.toByteArray();
        return Base64.encodeToString(fileByte, Base64.DEFAULT);
    }

    /****
     * For check value is exist in string or not
     * */
    public static String checkValue(String strValue, String strAddValue) {

        if (!TextUtils.isEmpty(strAddValue) && !"null".equalsIgnoreCase(strAddValue) && !strValue.contains(strAddValue)) {
            strValue += strAddValue+", ";
        }

        return strValue;
    }

}
