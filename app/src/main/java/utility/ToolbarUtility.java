package utility;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.encureit.trading.R;


/**
 * Created by amol on 31/7/17.
 */
@SuppressWarnings("ALL")
public class ToolbarUtility {
   public ToolbarUtility(){}

   public ToolbarUtility(final AppCompatActivity appCompatActivity, Toolbar toolbar){
       appCompatActivity.setSupportActionBar(toolbar);
       appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
       toolbar.setNavigationIcon(appCompatActivity.getResources().getDrawable(R.drawable.icon_back_navigation));
       toolbar.setTitleTextColor(appCompatActivity.getResources().getColor(R.color.white));
       toolbar.setNavigationOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               appCompatActivity.onBackPressed();
               appCompatActivity.finish();
           }
       });
   }
   public ToolbarUtility(final AppCompatActivity appCompatActivity, Toolbar toolbar, String string){
       appCompatActivity.setSupportActionBar(toolbar);
       appCompatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       appCompatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
       toolbar.setNavigationIcon(appCompatActivity.getResources().getDrawable(R.drawable.icon_back_navigation));
       toolbar.setTitleTextColor(appCompatActivity.getResources().getColor(R.color.white));
       appCompatActivity.getSupportActionBar().setTitle(string);
       toolbar.setNavigationOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               appCompatActivity.onBackPressed();
               appCompatActivity.finish();
           }
       });
   }


}
