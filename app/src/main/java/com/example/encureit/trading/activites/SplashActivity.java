package com.example.encureit.trading.activites;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;


import com.example.encureit.trading.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import utility.SharePreferenceSingleton;
import utility.loading_indicator.AVLoadingIndicatorView;

/**
 * Created by amol on 27-12-17.
 */
@SuppressWarnings("ALL")
public class SplashActivity extends Activity {

    private final String TAG = this.getClass().getSimpleName();
    @BindView(R.id.imageView)
    ImageView imageView;
    private SharePreferenceSingleton objSPS;
    private Context mContext = this;
    @BindView(R.id.customLoading)
    AVLoadingIndicatorView customLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        BaseActivity.printLog(TAG, "onCreate invoked!");

        objSPS = SharePreferenceSingleton.getSingletonInstance();
        objSPS.setPref(this);
        objSPS.setEditor1();

    }

    @Override
    protected void onStart() {
        super.onStart();
        BaseActivity.printLog(TAG, "onStart invoked!");
    }

    @Override
    protected void onResume() {
        super.onResume();
        BaseActivity.printLog(TAG, "onResume invoked!");
        customLoading.setIndicator("com.encureit.app.utilities.loading_indicator.MyCustomIndicator");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                customLoading.smoothToHide();

                    startActivity(new Intent(mContext, MainActivity.class));

                finish();
            }
        }, 3000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BaseActivity.printLog(TAG, "onPause invoked!");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BaseActivity.printLog(TAG, "onDestroy invoked!");
    }
}
