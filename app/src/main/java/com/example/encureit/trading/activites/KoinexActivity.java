package com.example.encureit.trading.activites;

import android.app.NotificationManager;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.encureit.trading.R;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import adapter.CustomPriceAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import custom_font.ButtonRegular;
import custom_font.EditTextRegular;
import custom_font.TextViewRegular;
import model.Price;
import model.PriceList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import utility.CustomProgressDialog;
import utility.SmsListener;
import utility.SmsReceiverClass;
import utility.ToolbarUtility;
import webUtil.APIServiceFactory;
import webUtil.AccountService;
import webUtil.AccountServiceProvider;

public class KoinexActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_BTC)
    TextViewRegular tvBTC;
    @BindView(R.id.tv_ETH)
    TextViewRegular tvETH;
    @BindView(R.id.tv_BCH)
    TextViewRegular tvBCH;
    @BindView(R.id.tv_XRP)
    TextViewRegular tvXRP;
    @BindView(R.id.tv_LTC)
    TextViewRegular tvLTC;
    @BindView(R.id.tv_MIOTA)
    TextViewRegular tvMIOTA;
    @BindView(R.id.tv_OMG)
    TextViewRegular tvOMG;
    @BindView(R.id.tv_GNT)
    TextViewRegular tvGNT;

    private final String TAG = MainActivity.class.getSimpleName();
    @BindView(R.id.edit_setPrice)
    EditTextRegular editSetPrice;
    @BindView(R.id.btn_save)
    ButtonRegular btnSave;
    @BindView(R.id.tv_Price)
    TextViewRegular tvPrice;
    @BindView(R.id.li_layout_trade)
    LinearLayout liLayoutTrade;
    private Context mContext = this;
    public AccountServiceProvider accountServiceProvider;
    private ArrayList<Price> priceList;
    private CustomPriceAdapter mAdapter;
    private AccountService accountService;
    private double xrp;
    private double setPrice;
    CustomProgressDialog customProgressDialog;
    public static final String OTP_REGEX = "[0-9]{1,6}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_koinex);
        ButterKnife.bind(this);
        new ToolbarUtility(this, toolbar, "Koinex");
        toolbar.setVisibility(View.VISIBLE);
        accountServiceProvider = new AccountServiceProvider(mContext);
        accountService = APIServiceFactory.createService(AccountService.class, mContext);
        priceList = new ArrayList<>();
        //  getData();
        customProgressDialog=CustomProgressDialog.getInstance();

        SmsReceiverClass.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                //From the received text string you may do string operations to get the required OTP
                //It depends on your SMS format
                Log.e("Message",messageText);
                Toast.makeText(KoinexActivity.this,"Message: "+messageText, Toast.LENGTH_LONG).show();

                Pattern pattern = Pattern.compile(OTP_REGEX);
                Matcher matcher = pattern.matcher(messageText);
                String otp="";
                while (matcher.find())
                {
                    otp = matcher.group();
                }

                Toast.makeText(KoinexActivity.this,"OTP: "+ otp ,Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        final long period = 50000;
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                // do your task here

                customProgressDialog.showDialog(mContext,"Loading...",0);
                getKoinexDetails();
                Log.d(TAG,"Thread called");

            }
        }, 0, period);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editSetPrice.getText().toString() != " ") {
                    setPrice = Double.parseDouble(editSetPrice.getText().toString());
                    editSetPrice.setText(" ");
                    tvPrice.setText("Set Price is :" + setPrice);
                } else {
                    BaseActivity.showToast(mContext, "Please enter Price");
                }
            }
        });



    }

    public void getKoinexDetails() {

        Log.d(TAG,"GetKoinexDetails called");
        accountService = APIServiceFactory.createService(AccountService.class, mContext);
        Call<PriceList> call = accountService.getKoinexPrice();
        call.enqueue(new Callback<PriceList>() {
            @Override
            public void onResponse(Call<PriceList> call, Response<PriceList> response) {
                if (response.isSuccessful() && response.body() != null) {
                    customProgressDialog.dismissDialog();
                    liLayoutTrade.setBackgroundColor(getResources().getColor(R.color.text_color));
                    Price price = response.body().getPrices();

                    tvBTC.setText(price.getBTC());
                    tvBCH.setText(price.getBCH());
                    tvXRP.setText(price.getXRP());
                    tvLTC.setText(price.getLTC());
                    tvETH.setText(price.getETH());
                 /*   tvMIOTA.setText((int) price.getMIOTA());
                    tvGNT.setText((int) price.getGNT());
                    tvOMG.setText((int) price.getOMG());*/

                    xrp = Double.parseDouble(price.getXRP());
                    if (setPrice != 0.0) {
                        if (xrp > setPrice) {
                            sendNotification(tvBCH);
                            BaseActivity.showToast(mContext, "Price is above " + setPrice);
                        }
                    }

                } else {

                    BaseActivity.showToast(mContext, "something went wrong");

                }

                liLayoutTrade.setBackgroundColor(getResources().getColor(R.color.white));
            }

            @Override
            public void onFailure(Call<PriceList> call, Throwable t) {
                BaseActivity.showToast(mContext, "error");
                customProgressDialog.dismissDialog();

            }
        });


    }


    public void sendNotification(View view) {

        //Get an instance of NotificationManager//
        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.icon_profile_camera)
                        .setContentTitle("Trading")
                        .setContentText("XRP Price :" + xrp).setSound(soundUri);


        // Gets an instance of the NotificationManager service//

        NotificationManager mNotificationManager =

                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // When you issue multiple notifications about the same type of event,
        // it’s best practice for your app to try to update an existing notification
        // with this new information, rather than immediately creating a new notification.
        // If you want to update this notification at a later date, you need to assign it an ID.
        // You can then use this ID whenever you issue a subsequent notification.
        // If the previous notification is still visible, the system will update this existing notification,
        // rather than create a new one. In this example, the notification’s ID is 001//


        if (mNotificationManager != null) {
            mNotificationManager.notify(001, mBuilder.build());
        }
    }


}
