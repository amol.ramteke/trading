package com.example.encureit.trading.activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

/**
 * Created by amol on 27/12/17.
 */

public class BaseActivity extends AppCompatActivity {

    private static final boolean DEBUG_ENABLE = true;
    private BroadcastReceiver mNetworkChangeService;
    public static void printLog(String tag, String message) {
        if (DEBUG_ENABLE) {
            Log.d(tag, message);
        }
    }

    public void printError(String tag, String message, Exception e) {
        if (DEBUG_ENABLE) {
            Log.e(tag, message, e);
        }
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    /*public static void showSnackBar(View view, String msg) {
        if (view != null) {
         //   Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).show();
        }
    }*/

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if (info == null) return false;
        NetworkInfo.State network = info.getState();
        return (network == NetworkInfo.State.CONNECTED || network == NetworkInfo.State.CONNECTING);
    }

    public static void hideKeyboard(Context context, View view) {
        // Check if no view has focus:
        //View view = context.get
        if (view != null) {

            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        }
    }


    private void registerNetworkBroadcastForNougat() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkChangeService, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            Log.e("BaseActivity","BaseActivity registerNetworkBroadcastForNougat called Nougat ");
        } if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(mNetworkChangeService, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            Log.e("BaseActivity","BaseActivity registerNetworkBroadcastForNougat called MarshMallow ");
        }

        Log.e("BaseActivity","BaseActivity registerNetworkBroadcastForNougat called");
    }

    protected void unregisterNetworkChanges() {
        try {
     //       unregisterReceiver(mNetworkChangeService);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        Log.e("BaseActivity","BaseActivity unregisterNetworkChanges called");
    }


    @Override
    protected void onStart() {
        super.onStart();
      //  mNetworkChangeService=new NetworkChangeService();
        //registerNetworkBroadcastForNougat();
        Log.e("BaseActivity","BaseActivity onStart called");
    }


    @Override
    protected void onStop() {
        super.onStop();
     //   unregisterNetworkChanges();
        Log.e("BaseActivity","BaseActivity onStop called");
    }



}
