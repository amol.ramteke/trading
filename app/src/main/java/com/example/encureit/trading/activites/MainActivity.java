package com.example.encureit.trading.activites;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.encureit.trading.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import custom_font.TextViewBold;
import custom_font.TextViewRegular;
import nav_drawer.NavItemClicked;
import nav_drawer.NavigationDrawerAdapter;
import utility.SharePreferenceSingleton;


public class MainActivity extends BaseActivity implements NavItemClicked {

    @BindView(R.id.tvMainToolbarTitle)
    TextViewBold tvMainToolbarTitle;
    @BindView(R.id.toolbarMain)
    Toolbar toolbarMain;
    @BindView(R.id.layout_container)
    FrameLayout layoutContainer;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    @BindView(R.id.imageViewUserProfile)
    ImageView imageViewUserProfile;
    @BindView(R.id.txt_NavTitle)
    TextViewBold txt_NavTitle;
    @BindView(R.id.txt_NavCity)
    TextViewRegular txt_NavCity;
    @BindView(R.id.rvForNavigation)
    RecyclerView rvForNavigation;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    private final String TAG = MainActivity.class.getSimpleName();
    private Context mContext = this;
    private SharePreferenceSingleton objSPS;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        printLog(TAG, "onCreate invoked!");
        ButterKnife.bind(this);


        setDrawerToggle();
    }

    @Override
    protected void onStart() {
        super.onStart();
        printLog(TAG, "onStart invoked!");
    }

    @Override
    protected void onResume() {
        super.onResume();
        printLog(TAG, "onResume invoked!");

        txt_NavTitle.setText(mContext.getResources().getString(R.string.app_name));
        txt_NavCity.setText(mContext.getResources().getString(R.string.register));
        imageViewUserProfile.setImageResource(R.drawable.icon_user_place_holder);
    }

    // For actionbar drawer toggle button
    private void setDrawerToggle() {

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbarMain,
                R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                printLog(TAG, "onDrawerClosed invoked!");
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                hideKeyboard(mContext, drawerView);
                printLog(TAG, "onDrawerOpened invoked!");
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    coordinatorLayout.setTranslationX(slideOffset * drawerView.getWidth());

                }
                drawerLayout.bringChildToFront(drawerView);
                drawerLayout.requestLayout();
            }
        };
        drawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        navigationItem();
    }

    // for set recyclerView in side drawer
    private void navigationItem() {
        String title[];
        int icon[];

        title = new String[]{
                mContext.getResources().getString(R.string.koinex),
                mContext.getResources().getString(R.string.zebpay),
                mContext.getResources().getString(R.string.coinsecure),
                mContext.getResources().getString(R.string.btcxindia),
                mContext.getResources().getString(R.string.menu_logout)};
        icon = new int[]{R.drawable.koinex_logo, R.drawable.zebpay_logo, R.drawable.coinsecure_logo,
                R.drawable.btcxindia_logo, R.mipmap.ic_launcher};

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        rvForNavigation.setLayoutManager(layoutManager);
        NavigationDrawerAdapter navAdaptor = new NavigationDrawerAdapter(mContext, this, title, icon);
        rvForNavigation.setAdapter(navAdaptor);
    }

    @Override
    protected void onPause() {
        super.onPause();
        printLog(TAG, "onPause invoked!");
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            //CustomMethods.backExit(mContext);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        printLog(TAG, "onDestroy invoked!");
    }

    @Override
    public void navItemClicked(String name, int position) {
        drawerLayout.closeDrawer(Gravity.START);
        switch (position) {
            case 0:startActivity(new Intent(MainActivity.this,KoinexActivity.class));
                break;
            case 1:startActivity(new Intent(MainActivity.this,MsgActivity.class));
                break;
            case 2:startActivity(new Intent(MainActivity.this,ConfirmPattern.class));
                break;
            case 3:startActivity(new Intent(MainActivity.this,MainActivity.class));
                break;
            case 4:
                //CustomMethods.showLogoutAlert(mContext);
                break;
        }
    }

}
