package com.example.encureit.trading.activites;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.encureit.trading.R;

import java.util.ArrayList;
import java.util.List;

import adapter.CustomMsgAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MsgActivity extends AppCompatActivity {


    public CustomMsgAdapter msgAdapter;
    public static final String TAG = MsgActivity.class.getSimpleName();
    public Context mContext;
    @BindView(R.id.msgList)
    RecyclerView msgList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_msg);
        ButterKnife.bind(this);


        Uri mSmsQueryUri = Uri.parse("content://sms/inbox");
        List<String> messages = new ArrayList<String>();

        Cursor cursor = null;
        try {
            cursor = getContentResolver().query(mSmsQueryUri, null, null, null, null);
            if (cursor == null) {
                Log.i(TAG, "cursor is null. uri: " + mSmsQueryUri);

            }
            for (boolean hasData = cursor.moveToFirst(); hasData; hasData = cursor.moveToNext()) {
                final String body = cursor.getString(cursor.getColumnIndexOrThrow("body"));
                if (body.contains("Dear"))
                        messages.add(body);
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        } finally {
            cursor.close();
        }
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        msgList.setLayoutManager(mLayoutManager);
        msgList.setAdapter(new CustomMsgAdapter(MsgActivity.this, R.layout.layout_for_msg, messages));


    }
}
