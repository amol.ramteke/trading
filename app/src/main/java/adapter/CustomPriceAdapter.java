package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.encureit.trading.R;

import java.util.List;

import model.Price;

/**
 * Created by EncureIT on 27-12-2017.
 */

public class CustomPriceAdapter  extends RecyclerView.Adapter<CustomPriceAdapter.MyViewHolder>{

   public List<Price> priceList;
   public Context context;
   public LayoutInflater mInflater;
    private final int layoutId;

    public CustomPriceAdapter(List<Price> priceList, Context context,int layoutId) {

        this.priceList = priceList;
        this.context = context;
        this.layoutId=layoutId;
        this.mInflater=LayoutInflater.from(context);

    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(mInflater.inflate(R.layout.layout_price_adapter,parent, false));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Price price=priceList.get(position);
        holder.tv_BTC.setText(price.getBTC());
        holder.tv_ETH.setText(price.getETH());
        holder.tv_BCH.setText(price.getBCH());
        holder.tv_XRP.setText(price.getXRP());
        holder.tv_LTC.setText(price.getLTC());
        holder.tv_MIOTA.setText(price.getBTC());
        holder.tv_OMG.setText(price.getBTC());
        holder.tv_GNT.setText(price.getBTC());
    }


    @Override
    public int getItemCount() {
        return priceList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        private final TextView tv_BTC;
        private final TextView tv_ETH;
        private final TextView tv_BCH;
        private final TextView tv_XRP;
        private final TextView tv_LTC;
        private final TextView tv_MIOTA;
        private final TextView tv_OMG;
        private final TextView tv_GNT;


        public MyViewHolder(View itemView) {
            super(itemView);

            tv_BTC= itemView.findViewById(R.id.tv_BTC);
            tv_ETH= itemView.findViewById(R.id.tv_ETH);
            tv_BCH=itemView.findViewById(R.id.tv_BCH);
            tv_XRP=itemView.findViewById(R.id.tv_XRP);
            tv_LTC=itemView.findViewById(R.id.tv_LTC);
            tv_MIOTA=itemView.findViewById(R.id.tv_MIOTA);
            tv_OMG=itemView.findViewById(R.id.tv_OMG);
            tv_GNT=itemView.findViewById(R.id.tv_GNT);


        }
    }



}
