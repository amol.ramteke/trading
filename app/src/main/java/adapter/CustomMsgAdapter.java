package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.encureit.trading.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EncureIT on 25-01-2018.
 */

public class CustomMsgAdapter extends RecyclerView.Adapter<CustomMsgAdapter.MyMsgViewHolder> {

    public List<String> msgList;
    public Context mContext;
    public LayoutInflater mInflater;
    private  int layoutId;

    public CustomMsgAdapter( Context context, int layoutId,List<String> msgList) {
        this.msgList = msgList;
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.layoutId=layoutId;

    }

    @Override
    public MyMsgViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       //View view=mInflater.inflate(this.layoutId,parent,false);
       View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_for_msg,parent,false);
        return new MyMsgViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyMsgViewHolder holder, int position) {
        String name=msgList.get(position);
         holder.textMsg.setText(name);
    }

    @Override
    public int getItemCount() {
        return msgList.size();
    }






    class MyMsgViewHolder extends RecyclerView.ViewHolder{

        private final TextView textMsg;

        public MyMsgViewHolder(View itemView) {
            super(itemView);
            textMsg=itemView.findViewById(R.id.textMsg);
        }
    }
}
